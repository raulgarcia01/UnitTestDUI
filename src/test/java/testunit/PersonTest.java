package testunit;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.Calendar;
import java.util.Date;

import org.junit.Test;

import sv.gob.rnpn.test.Person;

/**
 * @author Raul Garcia
 * @version 1.0
 *
 * Pruebas unitarias para la clase Persona y DUI
 */
public class PersonTest extends Person {

    Calendar aux = Calendar.getInstance();

    /**
     * Pruebas de edad conforme fecha de nacimiento.
     *
     */
    @Test
    public void testEdad() {
        // caso cumple años hoy
        aux.add(Calendar.YEAR, -20);
        assertEquals(getEdad(aux.getTime()), 20);
        aux = Calendar.getInstance();

        // caso cumplio años
        aux.add(Calendar.MONTH, -4);
        aux.add(Calendar.YEAR, -25);
        assertEquals(getEdad(aux.getTime()), 25);
        aux = Calendar.getInstance();

        //caso cumplira años
        aux.add(Calendar.MONTH, 1);
        aux.add(Calendar.YEAR, -30);
        assertEquals(getEdad(aux.getTime()), 29);
        aux = Calendar.getInstance();

        //caso enviandole null como fecha
        assertEquals(getEdad(null), -1);

        //caso para verificar que siempre retorne un valor
        assertNotNull(getEdad(new Date()));

        //caso en el cual se quiere verificar la edad de una fecha futura
        aux.add(Calendar.YEAR, 10);
        assertEquals(getEdad(aux.getTime()), -2);
        aux = Calendar.getInstance();

        // aun no cumple un año
        aux.add(Calendar.MONTH, -1);
        assertEquals(getEdad(aux.getTime()), 0);

    }

    /**
     * Pruebas de validacion del DUI.
     *
     */
    @Test
    public void testDui() {
        // caso en el que el dui es valido
        assertEquals(isDuiValido("00016297-5"), true);

        //caso en el que la longitud del dui es incorrecto
        assertEquals(isDuiValido("0"), false);

        //caso en el que el dui no es valido
        assertEquals(isDuiValido("0016297-5"), false);

        // caso para verificar que siempre retorne valor
        assertNotNull(isDuiValido("0016297-5"));

        // caso con dui nulo
        assertEquals(isDuiValido(null), false);

        // caso con formato invalido de dui
        assertEquals(isDuiValido("00a-29*45"), false);
    }
}
