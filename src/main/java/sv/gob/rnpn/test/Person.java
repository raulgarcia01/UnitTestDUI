package sv.gob.rnpn.test;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Raul Garcia
 * @version 1.0
 *
 *
 */
public class Person {

    /**
     * Funcion calcula la edad de una persona a partir de su fecha de
     * nacimiento.
     *
     * @param fechaNaci Representa la fecha de nacimiento de la persona
     * @return int Retorna un valor entero de la edad de la persona
     *
     */
    public int getEdad(Date fechaNaci) {
        int edad = 0;
        Calendar hoy = Calendar.getInstance();
        try {
            if (fechaNaci.before(new Date())) {
                Calendar fechaNacimiento = Calendar.getInstance();
                fechaNacimiento.setTime(fechaNaci);
                int anio = hoy.get(Calendar.YEAR) - fechaNacimiento.get(Calendar.YEAR);
                int mes = hoy.get(Calendar.MONTH) - fechaNacimiento.get(Calendar.MONTH);
                int dia = hoy.get(Calendar.DATE) - fechaNacimiento.get(Calendar.DATE);
                edad = (mes < 0 || (mes == 0 && dia < 0)) ? --anio : anio;
            } else {
                edad = -2;
            }
        } catch (java.lang.NullPointerException e) {
            edad = -1;
            System.out.print(e.getMessage());
        }
        return edad;
    }

    /**
     * Funcion que verifica si el dui de una persona es valido
     *
     * @param Dui Dui de la persona
     * @return boolean Retorna si el dui ingresado es valido
     *
     */
    public boolean isDuiValido(String Dui) {
        boolean resp = false;
        int suma = 0, division = 0, resta = 0;
        if (validateformatDui(Dui)) {
            for (int i = 0; i < 8; i++) {
                suma = suma + (9 - i) * Integer.parseInt(String.valueOf(Dui.charAt(i)));
            }

            division = suma % 10;
            resta = 10 - division;
            resp = (Integer.decode(String.valueOf(Dui.charAt(9))) == resta) ? true : false;
        }
        return resp;
    }

    /**
     * Funcion que verifica que el formato del Dui sea valido
     *
     * @param Dui Dui de la persona
     * @return boolean Retorna si el formato del dui es valido
     *
     */
    public boolean validateformatDui(String Dui) {
        boolean resp = false;
        try {
            Pattern validation = Pattern.compile("^\\d{8}-\\d$");
            Matcher matcher = validation.matcher(Dui);
            if (matcher.matches()) {
                resp = true;
            }
        } catch (java.lang.StringIndexOutOfBoundsException e) {
            System.out.print(e.getMessage());
        } catch (java.lang.NullPointerException ex) {
            System.out.print(ex.getMessage());
        }

        return resp;
    }

    /**
     * Convierte un string a Date
     *
     * @param date Fecha de la persona String en formato "dd/MM/YYYY"
     * @return Date Fecha en tipo de dato Date
     *
     */
    public static Date convertStringAFecha(String date) {
        Date fecha = null;
        if (date != null) {
            try {
                SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
                fecha = sdf.parse(date);
            } catch (Exception e) {
                fecha = null;
            }
        }
        return fecha;
    }

    /**
     * getFechaactual : obtiene la fecha actual
     *
     * @return String : retorna la fecha actual
     *
     */
    public static String getFechactual() {
        Calendar calendar = Calendar.getInstance();
        return convertFechaAString(calendar.getTime());
    }

    /**
     * convertFechaAString : convierte una fecha de Date a String
     *
     * @return String : convierte una fecha de Date a String en formato
     * "dd/MM/yyyy"
     *
     */
    public static String convertFechaAString(Date date) {
        String fecha = "";
        if (date != null) {
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
            fecha = sdf.format(date);
        }
        return fecha;
    }
}
